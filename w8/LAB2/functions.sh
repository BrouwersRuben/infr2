#!/bin/bash
# Function: Stores functions used in lab2.sh script
# Name: functions.sh
# Author: Ruben Brouwers

function showGreen {
 local reset='[0m' # local scope (function)
 local green='[0;32m'
 echo -e "\e$green $1 \e$reset"
}

function showRed {
 local reset='[0m' # local scope (function)
 local red='[0;31m'
 echo -e "\e$red $1 \e$reset"
}

function mysqlClientInstalled {
  if ! command -v mysql &> /dev/null
  then
      showRed "$mysqlError"
      exit 1
  fi
}

function createSQLInstance {
  gcloud sql instances create "${sqlName}" --region=europe-west1 --authorized-networks=0.0.0.0/0 --database-version=MYSQL_8_0 --root-password="${rootPassword}" --quiet

  if [ $? -eq 0 ]; then
      showGreen "$databaseSuccess"
  else
      showRed "$databaseFail"
      exit 1
  fi
}

function createUserSQL {
  gcloud sql users create wordpress --instance="${sqlName}" --password="${userPassword}" --quiet

  if [ $? -eq 0 ]
  then
      showGreen "$userMadeSuccess"
  else
      showRed "$userMadeFail"
      exit 1
  fi
}

function createVMInstance {
  gcloud compute instances create-with-container "${vmName}" --zone=europe-west1-b --tags=wordpress --container-image=wordpress:latest --container-env=[WORDPRESS_DB_HOST="$ipAddress", WORDPRESS_DB_USER="wordpress", WORDPRESS_DB_PASSWORD="$userPassword", WORDPRESS_DB_NAME="$sqlName"] --quiet
  if [ $? -eq 0 ]; then
      showGreen "$vmMadeSuccess"
  else
      showRed "$vmMadeFail"
      exit 1
  fi
}

function assignIPAdress {
  gcloud sql instances patch $sqlName --authorized-networks=$ipAddress --quiet

  if [ $? -eq 0 ]; then
      showGreen "$assignIPSuccess"
  else
      showRed "$assignIPFail"
      exit 1
  fi
}

function deleteVMInstance {
  if gcloud compute instances list --filter="${vmName}"
  then
      gcloud compute instances delete "${vmName}" --quiet
      if [ $? -eq 0 ]; then
              showGreen "$vmDeleteSuccess"
          else
              showRed "$vmDeleteFail"
              exit 1
          fi
  else
      showRed "$vmAlert"
  fi
}

function deleteSQLInstance {
  if gcloud sql instances list --filter="${sqlName}"
  then
      gcloud sql instances delete "${sqlName}" --quiet
      if [ $? -eq 0 ]; then
          showGreen "$sqlDeleteSuccess"
      else
          showRed "$sqlDeleteFail"
          exit 1
      fi
  else
      showRed "$dbAlert"
  fi
}