#!/bin/bash
# task: lab2
# name: lab2.sh
# author: Ruben Brouwers

suffix=$(date +%s)
vmName="wordpress-vm${suffix}"
sqlName="wordpress-sql${suffix}"

vmAlert="${vmName} does not exist"
dbAlert="${sqlName} does not exist"
mysqlError="MySql is not installed"
giveRootPassword="
Please give the root password for the database:"
databaseSuccess="
[SUCCESS] The database ${sqlName} is made.
"
databaseFail="
[FAILED] The database ${sqlName} is NOT made
"
giveUserPassword="
Give the password for DB user 'wordpress':"
userMadeSuccess="
[SUCCESS] The user 'wordpress' is made.
"
userMadeFail="
[FAILED]The user 'wordpress' is not made.
"
vmMadeSuccess="
[SUCCESS] The VM ${vmName} is made.
"
vmMadeFail="
[FAILED] The VM ${vmName} is not made.
"
extractIPSuccess="
[SUCCESS] The IP address: ${ipAddress} from VM instance ${vmName} extracted successfully
"
extractIPFail="
[FAILED] Getting IP address of ${vmName} failed
"
assignIPSuccess="
[SUCCESS] The IP address: ${ipAddress} is successfully assigned to ${sqlName}
"
assignIPFail="
[FAILED] Assigning IP address failed
"
vmDeleteSuccess="
[SUCCESS] ${vmName} has successfully been deleted
"
vmDeleteFail="
[FAILED] Deletion of ${vmName} failed
"
sqlDeleteSuccess="
[SUCCESS] ${sqlName} has successfully been deleted
"
sqlDeleteFail="
[FAILED] Deletion of ${sqlName} failed
"

source ./functions.sh

if [[ $1 = "-d" || $1 = "--delete" ]]
    then
        deleteVMInstance
        deletefireWallRule
        exit 0
fi

# Main
# Check if MySQL is installed
mysqlClientInstalled

# Ask the user for the root password of the SQL instance
echo -e "$giveRootPassword"
read -s rootPassword

# Create SQL instance
createSQLInstance

# Make a user with all permissions
echo -e "$giveUserPassword"
read -s userPassword

createUserSQL

# Extracting IP from SQL instance
ipAddress=$(gcloud sql instances describe "${sqlName}" --format='get(networkInterfaces[0].accessConfigs[0].natIP)')

if [ $? -eq 0 ]; then
    showGreen "$extractIPSuccess"
else
    showRed "$extractIPFail"
    exit 1
fi

# Creating VM instance
createVMInstance

