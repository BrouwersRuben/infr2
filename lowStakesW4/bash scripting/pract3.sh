#!/bin/bash

#------------------------------------------------------------------------------
#- Name		: pract3.sh
#- Alias	: -
#- Commit	: Practice Assignment creation of vm instance
#- Descr	: Create a VM on gcloud with additional services;
#-		: based on Ubuntu 20.04;
#-		: Opens up port 3000 in firewall rules.
#- Usage	: If '-d' or '--delete', then VM and firewall rule will be deleted
#------------------------------------------------------------------------------

suffix=$(date +%s)
vmname="pract3-${suffix}"
machinetype=n1-standard-1
zone=europe-west1-b
imgproj=ubuntu-os-cloud
imgfam=ubuntu-minimal-2004-lts
tags="chat"
rulename=http3000
portnr=3000

# Function: create VM
function makeVM () {
gcloud compute instances create ${vmname} \
--machine-type=${machinetype} \
--zone=${zone} \
--image-project=${imgproj} \
--image-family=${imgfam} \
--tags=${tags} \
--metadata startup-script='#!/bin/bash
        snap install rocketchat-server'
}

#Function: open firewall port:
function openFW () {
	if gcloud compute firewall-rules list |grep "${rulename}" &>/dev/null
	then
		echo "Firewall rule "${rulename}" already exists."
	else
		gcloud compute firewall-rules create ${rulename} --allow=tcp:"${portnr}" --target-tags="${tags}"
	fi
}

#Function: remove firewall rule:
function deleteFWPort () {
	# Check number of instances using a certain tag.
	tags_count=$(gcloud compute instances list --format='table(tags.list())' |grep "${tags}" |wc -l)
	if [[ "${tags_count}" -ge 2 ]]
	then
		echo "There are still instances using that port."
	elif [[ "${tags_count}" -eq 1 ]]
	then
		gcloud compute firewall-rules delete "${rulename}" --quiet
	else
		echo "Firewall rule ${rulename} does not exist."
	fi
}

#Function: delete VM:
function deleteVM () {
	if gcloud compute instances list --filter="${vmname}"; then
		gcloud compute instances delete "${vmname}" --quiet
	else
		echo "${vmname} does not exist."
	fi
}

#Main
if [[ $1 = "-d" || $1 = "--delete" ]]
then
	gcloud compute instances list
	echo -n "Enter the name of the vm : "
	read vmname
	deleteFWPort
	deleteVM
else
	echo "Creating VM and adding firewall rules."
	makeVM
	openFW
fi

IFS=$OLDIFS
