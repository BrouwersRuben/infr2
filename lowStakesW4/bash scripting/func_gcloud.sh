#!/bin/bash

#--------------------------------------------------
#- Name     : /home/student/acs2infra/gcloud
#- Commit   : Function definitions for gcloud
#- Descr    : Function definitions for gcloud
#- Usage    : Include in scripts with 'source'
#-------------------------------------------------

# Function: create VM
function makeVM {
gcloud compute instances create "${vmname}" \
--machine-type="${machinetype}" \
--zone="${zone}" \
--image-project="${imgproj}" \
--image-family="${imgfam}" \
--tags="${tags}" \
--metadata startup-script="${startup_script}"
}

# Function: Open firewall port
function openFW {
    if gcloud compute firewall-rules list |grep "${rulename}" &>/dev/null
    then
        echo "Firewall rule "${rulename}" already exists."
    else
        gcloud compute firewall-rules create "${rulename}" --allow=tcp:"${portnr}" --target-tags="${tags}"
    fi
}

# Function: remove firewall rule:
function deleteFWPort {
    # Check number of VMs using a certain tag.
    tags_count=$(gcloud compute instances list --format='table(tags.list())' |grep "${tags}" |wc -l)
    if [[ "${tags_count}" -ge 2 ]]
    then
        echo "There are still VMs using that port."
    elif [[ "${tags_count}" -eq 1 ]]
    then
        gcloud compute firewall-rules delete "${rulename}" --quiet
    else
        echo "Firewall rule ${rulename} does not exist"
    fi
}

# Function: delete VM
function deleteVM {
    if gcloud compute instances list --filter="${vmname}"
    then
        gcloud compute instances delete "${vmname}" --quiet
    else
        echo "${vmname} does not exist"
    fi
}

