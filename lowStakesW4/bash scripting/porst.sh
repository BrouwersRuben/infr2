#!/bin/bash

regex="^([[:alnum:]]+)[^:]+:+([[:alnum:]]+)"
echo "TCP ports:"

while read -r line; do
  [[ $line =~ $regex ]]
  if [ "${BASH_REMATCH[1]}" = "tcp" ]; then
    echo "${BASH_REMATCH[2]}"
  fi
done < <(netstat -tulpn 2> /dev/null)