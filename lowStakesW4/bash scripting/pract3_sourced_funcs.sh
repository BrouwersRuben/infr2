#!/bin/bash

#------------------------------------------------------------------------------
#- Name		: pract3.sh
#- Alias	: -
#- Commit	: Practice Assignment creation of vm instance; with sourced functions
#- Descr	: Create a VM on gcloud with additional services;
#-		    : based on Ubuntu 20.04;
#-		    : Opens up port 3000 in firewall rules.
#- Usage	: If '-d' or '--delete', then VM and firewall rule will be deleted
#------------------------------------------------------------------------------

source ./func_gcloud.sh

OLDIFS=$IFS
IFS='
'

suffix=$(date +%s)
vmname="pract3-${suffix}"
machinetype=n1-standard-1
zone=europe-west1-b
imgproj=ubuntu-os-cloud
imgfam=ubuntu-minimal-2004-lts
tags="chat"
rulename=http3000
portnr=3000
startup_script="#!/bin/bash
snap install rocketchat-server"

#Main
if [[ $1 = "-d" || $1 = "--delete" ]]
then
    gcloud compute instances list
    echo -n "Enter the name of the vm : "
    read vmname
    deleteFWPort
    deleteVM
else
    echo "Creating VM and adding firewall rules"
    makeVM
    openFW
fi

IFS=$OLDIFS
