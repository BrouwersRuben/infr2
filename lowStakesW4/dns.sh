#!/bin/bash
# name: dns.sh
# author: Ruben Brouwers
# function: displays the name of the servers accessed when searching for a url

default_value="canvas.kdg.be"

count=1
time=0

resetColor='[0m'
greenColor='[0;32m'

# ;; Received 811 bytes from 127.0.0.53#53(127.0.0.53) in 5 ms
regex="(\([[:alnum:]]+\))+ in +(\.)$"

help="
Name: dns.sh\n
Function: Display the servers accessed whe, searching for a url.\n
\n
OPTIONS:\n
--help show the help options\n
\n
"

noParam="
There is no paramter given, used url: $default_value\n
"

paramGiven="
You gave the url: $1\n
"

totalTime="Total time: "

function showOutput {
	echo -e "NameServer$1: \e$greenColor $2 \e$resetColor replied in $3 ms"
}


function doStuff {
	while read -r line
		do
  			[[ $line =~ $regex ]]
  			# showOutput $count ${BASH_REMATCH[1]} ${BASH_REMATCH[2]}
  			time=($time+1)
  			showOutput $count testServer testTime
done < <(dig +trace $1 2> /dev/null)
echo -e $totalTime $time
}

function listenToUser {
	if [ "$1" = "--help" ]
		then
			echo -e $help
	elif [ "$1" = "" ]
		then
			echo -e $noParam
			url=${1:-$default_value}
			doStuff $url
	else
		echo -e $paramGiven
		doStuff $1
	fi
}

listenToUser $1	

# The bash rematch is not working and I could not get a simple calculation working, but the rest should be there...
