#include <stdio.h>

extern int fib(int i);

int main() {
	int i = 6;
	int f = fib(i);
	printf("fib(%d) = %d\n", i, f);
}

