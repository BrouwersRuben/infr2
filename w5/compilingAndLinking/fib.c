int fib(int i) {
	if (i < 3)
		return 1;
	else
		return fib(i-1) + fib(i-2);
}

int foo() {
	return fib(3);
}

