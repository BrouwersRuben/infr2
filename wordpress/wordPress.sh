#!/bin/bash
# Function: Create a Wordpress environment (Wordpress CMS on one VM, the mysql database on a database instance).
# Name: wordPress.sh
# Author: Ruben Brouwers

source ./wordPressFunctions.sh

# Variables
suffix=$(date +%s)
vmName="wordpress-vm${suffix}"
sqlName="wordpress-sql${suffix}"
startup_script='#!/bin/bash
apt update
apt install -y apache2 wordpress vim
rm -r /var/www/html
ln -s /usr/share/wordpress /var/www/html
cd /usr/share/wordpress
cp wp-config-sample.php wp-config.php
sed -i s/database_name_here/wordpress/g wp-config.php
sed -i s/username_here/wordpress/g wp-config.php
'

vmAlert="${vmName} does not exist"
dbAlert="${sqlName} does not exist"
mysqlError="MySql is not installed"
giveRootPassword="
Please give the root password for the database:"
databaseSuccess="
[SUCCESS] The database ${sqlName} is made.
"
databaseFail="
[FAILED] The database ${sqlName} is NOT made
"
giveUserPassword="
Give the password for DB user 'wordpress':"
userMadeSuccess="
[SUCCESS] The user 'wordpress' is made.
"
userMadeFail="
[FAILED]The user 'wordpress' is not made.
"
vmMadeSuccess="
[SUCCESS] The VM ${vmName} is made.
"
vmMadeFail="
[FAILED] The VM ${vmName} is not made.
"
extractIPSuccess="
[SUCCESS] The IP address: ${ipAddress} from VM instance ${vmName} extracted successfully
"
extractIPFail="
[FAILED] Getting IP address of ${vmName} failed
"
assignIPSuccess="
[SUCCESS] The IP address: ${ipAddress} is successfully assigned to ${sqlName}
"
assignIPFail="
[FAILED] Assigning IP address failed
"
vmDeleteSuccess="
[SUCCESS] ${vmName} has successfully been deleted
"
vmDeleteFail="
[FAILED] Deletion of ${vmName} failed
"
sqlDeleteSuccess="
[SUCCESS] ${sqlName} has successfully been deleted
"
sqlDeleteFail="
[FAILED] Deletion of ${sqlName} failed
"

# Main
# Check if MySQL is installed
mysqlClientInstalled

# Ask the user for the root password of the SQL instance
echo -e "$giveRootPassword"
read -s rootPassword

# Create SQL instance
createSQLInstance

# Make a user with all permissions
echo -e "$giveUserPassword"
read -s userPassword

createUserSQL

# Creating VM instance
createVMInstance

# Extracting IP from VM instance
ipAddress=$(gcloud compute instances describe "${vmName}" --format='get(networkInterfaces[0].accessConfigs[0].natIP)')

if [ $? -eq 0 ]; then
    showGreen "$extractIPSuccess"
else
    showRed "$extractIPFail"
    exit 1
fi

# Assigning extracted IP address to VM instance
assignIPAdress

# Waiting for 15 minutes until deleting both instances
sleep 15m #15 minutes

# Deleting instances
## VM instance
deleteVMInstance

## SQL instance
deleteSQLInstance