#!/bin/bash
# colorized arguments in functions

reset='[0m'
red='[0;31m'
green='[0;32m'

showColorizedWordsRed(){
  echo -e "\e$red $1 \e$reset"
}

showColorizedWordsGreen(){
  echo -e "\e$green $1 \e$reset"
}

showColorizedWordsRed "Hello"
showColorizedWordsGreen "Approximately red"