#!/bin/bash
# Name: practiceAssignment1.sh
# Function:  check if both parameters are present and if the parameters are integers. Let the function return the highest number.
# Author: Ruben Brouwers

reset='[0m'
red='[0;31m'
green='[0;32m'

function checkNumbersPresent(){
  if [ $# -eq 2 ]
    then
      max="$1"
      for param in "$@"
      do
        if [ "$param" -gt "$max" ]
        then
          max="$param"
        fi
      done
      echo "$max"
    else
      echo -e "\e$red No 2 parameters given \e$reset"
  fi
}

checkNumbersPresent 8 77