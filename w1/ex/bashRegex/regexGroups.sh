#!/bin/bash
# Name: bash_rematch_groups.sh
content="KdG University, Nationalestraat 5, B-2000 Antwerp"
regex="([ a-zA-Z]+), ([[:alpha:]]+) ([[:digit:]]+), (.*) (.*)"
[[ $content =~ $regex ]]
echo "Name: ${BASH_REMATCH[1]}"
echo "Street: ${BASH_REMATCH[2]} Nr: ${BASH_REMATCH[3]}"
echo "Zip code: ${BASH_REMATCH[4]} City: ${BASH_REMATCH[5]}"