#!/bin/bash
# Function : Check zip code with regex expression
# Name : regex_op.sh
content="KdG University, Nationalestraat 5,B-2000 Antwerp"
regex="B-[0-9]{4}"
if [[ $content =~ $regex ]]
  then
    echo "Zip code found" ;
    exit 0
  else
    echo "Zip code not found" >&2 ; exit 1
fi