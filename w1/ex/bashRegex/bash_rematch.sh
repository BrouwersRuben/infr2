#!/bin/bash
# Function: regex for a zip code
# Name : bash_rematch.sh

content="KdG University, Nationalestraat 5,B-2000 Antwerp"
regex="B-[0-9]{4}"

[[ $content =~ $regex ]]

echo "${BASH_REMATCH[0]}"