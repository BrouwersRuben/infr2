#!/bin/bash
# Name: substring.sh
# Function: extract substring from string
url="http://www.kdg.be/index.html"
echo String ${url}
echo From 7: ${url:7}
# Parentheses or space is needed to avoid confusion with ‘:-’
echo From -4: ${url: -4}
# or ${url:(-4)}
echo From offset 0, length 4: ${url:0:4}
echo From offset 7, length 10: ${url:7:10}