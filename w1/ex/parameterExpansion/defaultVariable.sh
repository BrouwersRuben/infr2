#!/bin/bash
# Function: Search large files, default > 10 MB
# Name : deflt_varvalue.sh
# Args:
Arg $1 MAY be supplied
# $1 gets a default value if empty
default_value="10"
size=${1:-$default_value} # OR =${1:-10}
find / -size "+${size}M" 2>/dev/null