#!/bin/bash
# Function: change case
# Name: casemod.sh
url="http://www.kdg.be/index.html"
upper="THIS IS ALL UPPER CASE"
echo First letter uppercase: ${url^}
echo First letter lowercase: ${upper,}
echo Everything uppercase: ${url^^}
echo Everything lowercase: ${upper,,}