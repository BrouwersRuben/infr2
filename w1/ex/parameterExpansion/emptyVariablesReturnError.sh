#!/bin/bash
# Function: Find large files
# Name: emptyvar.sh
# Args: Arg $1 MUST be supplied
# if $1 empty → exit code 1

size=${1?"Usage: $(basename $0) ARGUMENT"}
# Script executes the rest only if $1 provided
find . -name "*" -size "+${size}M"