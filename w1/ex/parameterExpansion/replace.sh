#!/bin/bash
# Function:
Replace in a string
# Name : search_replace.sh
url="http://www.kdg.be/index.html"
echo String ${url}
echo “Replace once ‘kdg’ with ‘student’: ${url/kdg/student}”
echo “Replace all occurrences of ‘ht’ with ‘f’: ${url//ht/f}”
echo “Replace at start ‘http’ with ‘ftp’: ${url/#http/ftp}”
echo “Replace at end ‘html’ with ‘aspx’: ${url/%html/aspx}”