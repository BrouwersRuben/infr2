#!/bin/bash
# Function: Remove matching prefix/suffix pattern greedy (longest match)
# and non-greedy (shortest match)
# Name: trimstring.sh
url="http://www.kdg.be/index.html"
echo "Remove shortest substring http*. at begin: ${url#http*.}"
echo "Remove longest substring http*. at begin: ${url##http*.}"
echo "Remove non-greedy .* at end: ${url%.*}"
echo "Remove greedy .* at end: ${url%%.*}"