#!/bin/bash
# Name : intvar_heredoc.sh
cat > child.sh <<!EOF
#!/bin/bash
while true; do
echo "child $$"
sleep 1
done
!EOF
chmod +x child.sh; ./child.sh &
sleep 2
echo "Last argument: $_"
# In this case: 2
kill $!
# PID last background process (child.sh)
kill $$
# PID current process