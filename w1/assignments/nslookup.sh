#!/bin/bash
# Function: Convert IP addresses to names
# Name: nslookup.sh
# Author: Ruben Brouwers

inputfile="ips.txt"
cat > $inputfile <<!
109.74.196.225
91.189.90.40
94.176.99.133
!

regexp="name = .{1,}"

function fun_nslookup {
  [[ $(nslookup $1 | head -n 1) =~ $regexp ]]
  result=${BASH_REMATCH[0]}
  echo "The name of IP address $1 is ${result:7}"
}

function readLines {
  for line in $(cat $inputfile)
  do
    fun_nslookup $line
  done
}

readLines

