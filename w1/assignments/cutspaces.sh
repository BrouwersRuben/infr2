#!/bin/bash
# Function: Replace spaces in file names with underscores.
# Name: cutspaces.sh
# Author: Ruben Brouwers
# Args: Provide the path where you would like to rename the files

function replaceSpacesWithUnderscores {
for files in $1/* # add * to specify files and not the folder
do
  mv "$files" "${files// /_}";
done
}

replaceSpacesWithUnderscores $1
