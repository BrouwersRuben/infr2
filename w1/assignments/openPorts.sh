#!/bin/bash
# Function: Find which process uses a certain port
# Name: openPorts.sh
# Author: Ruben Brouwers

help="
Warning:    execute this script as root or with sudo permissions.
Description:    This script displays which process uses a certain port.
Usage:        sudo ./netcat.sh [OPTIONS]

OPTIONS:
-h, --help    Display these instructions
-t, --testing    Start a listening port on 13 with 'netcat'
no option    Execute this script
\n
"

openedPort="
Port #13 is open for listening
\n
"

norootError="
You are not executing this script as root
\n
"

netcatNotFoundError="
netcat is not found on your system. Please install it using sudo apt install netcat.
\n
"

function checkRoot {
  if ! [ "$EUID" -eq 0 ]; then
      echo -e $norootError
      exit 1
  fi
}

function testingOption {
  if ! command -v netcat &> /dev/null
    then
      echo -e $netcatNotFoundError
      exit 2
  fi

  tpid=$(nc -l 13 &)
}

function listenUser {
  if [[ "$1" = '-t' || $1 = '--testing' ]]
    then
      testingOption
      echo -e $openedPort
  elif [ "$1" = "-h" ]
    then
      echo -e $help
      exit 0
    elif [ $1 == ""]
    then
      echo -e "No port given \n"
      exit 1
    else
      for (( i = 1; i <= 100; i++ ))
      #This is not fully working...
        do
          sudo lsof -nPi tcp:"$1" | grep -i "listen"
      done
  fi
}

function endScript {
    kill "$tpid" &> /dev/null
}


checkRoot
listenUser $1
endScript