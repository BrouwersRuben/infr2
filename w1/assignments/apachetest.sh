#!/bin/bash
# Function: benchmarking test for apache webserver
# Name: apachetest.sh
# Author: Ruben Brouwers

notInstalledError="The programs ab/curl are required. Install them with ‘sudo dnf install apache2-utils’ and ‘sudo dnf install curl’.
\n
"
installedSuccess="The required programs are installed
\n
"

url=$1
fallbackURL='127.0.O.1'

urlError="
You have not specified a url, so the script will use 127.0.0.1
\n
"

urlSuccess="
Your specified url is ${url}
\n
"

urlNotReachable="The url is not reachable.
\n
"
urlReachable="This URL is reachable
\n
"

function checkInstalledPrograms {
      # Check if ab and curl are installed,
      # if not --> stop with exit code 1
      # display notInstalledError

  command -v curl > /dev/null
  if [ $? -ne 0 ]
    then
      echo -e  $notInstalledError
      exit 1
  fi

  command -v ab > /dev/null
  if [ $? -ne 0 ]
    then
      echo -e $notInstalledError
      exit 1
  fi

  echo -e $installedSuccess
}

function checkURL {
    # Check if the user has supplied a url;
    # If not, use 127.0.O.1
    if [ -z $url ]
      then
        url=$fallbackURL
        echo -e $urlError
      else
        echo -e $urlSuccess
    fi
}

function checkURLReachable {
  # Check if the url is reachable, if not exit script
  if curl --head --silent --fail $url > /dev/null
   then
    echo -e $urlReachable
   else
    echo -e $urlNotReachable
    exit 1
  fi
}

function performBenchmark {
  # Do the benchmark
  ab -n 100 -kc 10 ${url}
}


function checkLastChar {
  # Checking if the last character of the url is a "/"
  if [ "${url: -1}" = "/" ]
    then
      performBenchmark
    else
      url=$url"/"
      performBenchmark
  fi
}

checkInstalledPrograms
checkURL
checkURLReachable
checkLastChar