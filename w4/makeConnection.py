#!/usr/bin/python3
import psycopg2
# Open the database connection
conn = psycopg2.connect (
dbname="postgres",

user='infr2',
password='infr2',
host='35.195.39.15')
# Prepare a cursor object
cur = conn.cursor()
# Execute this SQL query to retrieve the Postgres version
cur.execute("SELECT VERSION()")
# Fetch a single row
data = cur.fetchone()
print("Connection established to: ", data)
# Close the connection
conn.close()
