#!/usr/bin/python3

import psycopg2

try:
	conn = psycopg2.connect (
		dbname="postgres",
		user='infr2',
		password='infr2',
		host='35.195.39.15')
except:
	print("I am unable to connect to the database.")

cur = conn.cursor()

cur.execute("DROP TABLE IF EXISTS employees")

sql="""CREATE TABLE employees (
first_name VARCHAR(20) NOT NULL,
last_name VARCHAR(20),
age INT)
"""

insertData="""
INSERT INTO employees (first_name, last_name, age)
VALUES ('ruben', 'brouwers', 20)
"""

# Create the table
cur.execute(sql)

# Inserting some data
cur.execute(insertData)

# Commit the changes
conn.commit()

# Close the connection
conn.close()
