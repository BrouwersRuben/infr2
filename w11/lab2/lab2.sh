#!/bin/bash

paramError="You have not given the correct parameters"
notInstalledError="The program Rsync is not installed"
help="
Function: Transfer the current directory to a remote server using rsync
Options:
--help		Show the help dialog
--transfer	Transfer the files to the remote server
			-- give sever username
			-- give server host address
--dry-run	Do a dry run of rsync

Example:
./lab2.sh --transfer rubenbrouwers 172.1.1.1
"
dryRunMessage="
Running the script in Dry Run mode.
"
transferMessage="
Running the script in Production mode.
"

if [ $# -eq 0 ]
    then
        echo -e "$paramError";
elif [ $1 == "--dry-run" ]
  then
    echo -e "$dryRunMessage"
    rsync --dry-run -az --force --delete --progress --itemize-changes --exclude-from=rsync_ignore.txt ./ $2@$3/var/www/website
elif [ $1 == "--transfer" ]
	then
		command -v rsync > /dev/null
  		if [ $? -ne 0 ]
    			then
      				echo -e "$notInstalledError"
      				exit 1
  		fi
    echo -e "$transferMessage"
		rsync -az --force --delete --progress --itemize-changes --exclude-from=rsync_ignore.txt ./ $2@$3/var/www/website
elif [ $1 == "--help" ]
  then
	echo -e "$help"
fi
