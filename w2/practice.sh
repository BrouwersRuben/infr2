#!/bin/bash
# task: Gcloud SDK answers
# name: practive.sh
# author: Ruben Brouwers

# a. Red Hat VM Images
echo -e '\n Redhat images'

gcloud compute images list | grep rhel | wc -l

# b. Regions in europe
echo -e '\n Regions europe'

gcloud compute regions list | grep europe | wc -l

# Zones in region 'europe-north1'
echo -e '\n Zones in region europe-north1'

gcloud compute zones list | grep europe-north1 | wc -l

# Which machine has the most ram in zone 'europe-west-b'
echo -e '\n Which machine has the most ram in zone europe-west-b'

gcloud compute machine-types list | grep europe-west1-b | sort -n -r -k 4 | head -n 10
