#!/bin/bash
set -x
trap read debug
# Function: Returns a list of possible disk types in a defined region
# Name: pract0_disktypes.sh
# Author: Ruben Brouwers
# Args: type of disk (standard, ssd, balanced), region (us-central1-f, ...)

reset='[0m'
red='[0;31m'

error="
\e$red FOUND NOTHING \e$reset
\n
"

rootError="
This script should be ran as root
\n
"

noOptionsGiven="
You did not specify any options
\n
"

nep="
You need 2 parameters.
\n
"

help="
Warning : execute this script as a regular user.\n
Description : Use bash REMATCH to list available disk types and regions in gcloud \n
Usage : pract0_disktypes.sh DISKTYPE REGION \n
ARGUMENTS: \n
- DISKTYPE : choose from standard, ssd or balanced \n
- REGION : choose from asia, australia, europe, northamerica, southamerica, us \n
\n
"

if [[ "$1" = '-h' || $1 = '--help' ]]
  then
    echo -e $help
    exit 0
elif [ "$1" == "" ]
  then
    echo -e $noOptionsGiven
    exit 1
elif [ "$#" -lt 2 ]; then
      echo -e $nep
      exit 1
else
  if [[ $(gcloud compute disk-types list | egrep -i "$1[[:blank:]]{0,}$2") ]]
  then
    gcloud compute disk-types list | egrep -i "$1[[:blank:]]{0,}$2"
    exit 0
  else
    echo -e $error
    exit 1
  fi
fi